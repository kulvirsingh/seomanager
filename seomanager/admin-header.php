<?php
include("session.php");
include_once("../codelibrary/inc/seomanager_variables.php");
include_once("../codelibrary/inc/functions.php");
$obj= new database_class(); 
$func_obj = new common_function() ;
$ad= new ad_class();
$obj_paging = new paging;
$adminid=$_SESSION["sess_manager_id"];
$admin_rs = $obj->getAnyTableWhereData($obj->getTable("var_seomanager_login_table")," and id=".$_SESSION["sess_manager_id"]); 
/*$modules=$ad->get_module();
foreach($modules as $mod)
{
define($mod['name']."_MODULE",$mod['id']);
}*/
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


<!doctype html>
<html class="no-js" lang="">
<head>
<meta charset="utf-8">
<title>Find Your Professional : Admin Area</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width">
<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<!-- page level plugin styles -->
<link rel="stylesheet" href="styles/climacons-font.css">
<link rel="stylesheet" href="vendor/rickshaw/rickshaw.min.css">
<!-- /page level plugin styles -->
<link rel="stylesheet" href="vendor/summernote/dist/summernote.css">
<!-- /page level plugin styles -->
<!-- page level plugin styles -->
<!-- build:css({.tmp,app}) styles/app.min.css -->
<link rel="stylesheet" href="vendor/bootstrap/dist/css/bootstrap.css">
<link rel="stylesheet" href="vendor/perfect-scrollbar/css/perfect-scrollbar.css">
<link rel="stylesheet" href="styles/roboto.css">
<link rel="stylesheet" href="styles/font-awesome.css">
<link rel="stylesheet" href="styles/panel.css">
<link rel="stylesheet" href="styles/feather.css">
<link rel="stylesheet" href="styles/animate.css">
<link rel="stylesheet" href="styles/urban.css">
<link rel="stylesheet" href="styles/custom.css">
<link rel="stylesheet" href="styles/urban.skins.css">
<link rel="stylesheet" href="vendor/bootstrap-daterangepicker/daterangepicker-bs3.css">
<link rel="stylesheet" href="vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
<link rel="stylesheet" href="vendor/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
<link rel="stylesheet" href="scripts/jquery.dataTables.min.css">
<!-- endbuild -->


</head>
<body>
<div class="app layout-fixed-header">
<!-- sidebar panel -->
<div class="sidebar-panel offscreen-left">
<div class="brand"style="background-color: #ffffff;">
<!-- logo -->
<div class="brand-logo">
<img src="https://www.caonweb.com/images/website-logo.png" height="40" alt="">
</div>
<!-- /logo -->
<!-- toggle small sidebar menu -->
<a href="javascript:;" class="toggle-sidebar hidden-xs hamburger-icon v3" data-toggle="layout-small-menu">
 <span></span>
 <span></span>
<span></span>
 <span></span>
 </a>
 <!-- /toggle small sidebar menu -->
</div>
 <!-- main navigation -->
 <nav role="navigation">



  <div class="nav-md">
   
       
        <div class=" left_col">
          <div class="left_col scroll-view">
           

            

            <!-- sidebar menu -->
            <div id="sidebar-menu">
              
               
                <ul class="nav side-menu">
                  <li><a href="welcome.php"><i class="fa fa-tachometer"></i> Dashboard </a></li>
                  <!--<li><a href="service-manage.php"><i class="fa fa-user-secret"></i> Manage Services </a></li>-->
                  
				   <li><a><i class="fa fa-user-secret"></i> Manage Services Keyword <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
						<li><a href="service-keyword-manage.php">Add services keyword </a></li>
					</ul>
                  </li>

                   <li><a><i class="fa fa-user-secret"></i> Manage location Keyword <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
						<li><a href="service-manage.php">Add country keyword</a></li>
						<li><a href="service-manage.php">Add state keyword</a></li>
						<li><a href="service-manage.php">Add city keyword</a></li>
					</ul>
                  </li> 				  
				  
				
				  
				  
             
				  
				  
				  
				  
				  
                </ul>
             
               

            </div>
            <!-- /sidebar menu -->
 
            <!-- /menu footer buttons -->
          </div>
        </div>

        
        <!-- /footer content -->
      
   
    </div>
	
	
	



<script>
function init_sidebar() {
    var a = function() {
        $RIGHT_COL.css("min-height", $(window).height());
        var a = $BODY.outerHeight(),
            b = $BODY.hasClass("footer_fixed") ? -10 : $FOOTER.height(),
            c = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
            d = a < c ? c : a;
        d -= $NAV_MENU.height() + b, $RIGHT_COL.css("min-height", d)
    };
    $SIDEBAR_MENU.find("a").on("click", function(b) {
        var c = $(this).parent();
        c.is(".active") ? (c.removeClass("active active-sm"), $("ul:first", c).slideUp(function() {
            a()
        })) : (c.parent().is(".child_menu") ? $BODY.is(".nav-sm") && ($SIDEBAR_MENU.find("li").removeClass("active active-sm"), $SIDEBAR_MENU.find("li ul").slideUp()) : ($SIDEBAR_MENU.find("li").removeClass("active active-sm"), $SIDEBAR_MENU.find("li ul").slideUp()), c.addClass("active"), $("ul:first", c).slideDown(function() {
            a()
        }))
    }), $MENU_TOGGLE.on("click", function() {
        $BODY.hasClass("nav-md") ? ($SIDEBAR_MENU.find("li.active ul").hide(), $SIDEBAR_MENU.find("li.active").addClass("active-sm").removeClass("active")) : ($SIDEBAR_MENU.find("li.active-sm ul").show(), $SIDEBAR_MENU.find("li.active-sm").addClass("active").removeClass("active-sm")), $BODY.toggleClass("nav-md nav-sm"), a()
    }), $SIDEBAR_MENU.find('a[href="' + CURRENT_URL + '"]').parent("li").addClass("current-page"), $SIDEBAR_MENU.find("a").filter(function() {
        return this.href == CURRENT_URL
    }).parent("li").addClass("current-page").parents("ul").slideDown(function() {
        a()
    }).parent().addClass("active"), $(window).smartresize(function() {
        a()
    }), a(), $.fn.mCustomScrollbar && $(".menu_fixed").mCustomScrollbar({
        autoHideScrollbar: !0,
        theme: "minimal",
        mouseWheel: {
            preventDefault: !0
        }
    })
}

 

  
 
   
     ! (jQuery, "smartresize");
var CURRENT_URL = window.location.href.split("#")[0].split("?")[0],
    $BODY = $("body"),
    $MENU_TOGGLE = $("#menu_toggle"),
    $SIDEBAR_MENU = $("#sidebar-menu"),
    $SIDEBAR_FOOTER = $(".sidebar-footer"),
    $LEFT_COL = $(".left_col"),
    $RIGHT_COL = $(".right_col"),
    $NAV_MENU = $(".nav_menu"),
    $FOOTER = $("footer"),
    randNum = function() {
        return Math.floor(21 * Math.random()) + 20
    };
  
   $(document).ready(function() {
     init_sidebar()   
});
</script>








































 <!--<ul class="nav">
 
 <li id="wc">
<a href="welcome.php"><i class="fa fa-tachometer"></i><span>Dashboard</span></a>
</li>
 <li id="ad">
 <a href="service-manage.php"><i class="fa fa-user-secret"></i><span>Manage Services </span></a>
</li>

   <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="menutwo">
        <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionMenu" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          <i class="fa fa-user-secret"></i> Manage Location <span class="fl-right"> <i class="fa fa-caret-down" aria-hidden="true"></i></span>
        </a>
      </h4>
      </div>
      <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="menutwo">
        <div class="panel-body">
          <ul class="nav">
            <li id="co"><a href="country-manage.php" style="color: black;"><i class="fa fa-caret-right" aria-hidden="true"></i> Country</a></li>
            <li id="st" ><a href="state-manage.php" style="color: black;"><i class="fa fa-caret-right" aria-hidden="true"></i> State</a></li>
            <li id="ci"><a href="city-manage.php" style="color: black;"><i class="fa fa-caret-right" style="color: black;" aria-hidden="true"></i> City</a></li>
            
          </ul>
        </div>
      </div>
    </div>

 <li>
 <a href="logout.php"><i class="fa fa-sign-out"></i><span>Logout</span></a>
 </li> 
 </ul>--->
 
 
 
 
</nav>
 <!-- /main navigation -->
</div>
<!-- /sidebar panel -->
 <!-- content panel -->
  <div class="main-panel">
 <!-- top header -->
<header class="header navbar">
 <div class="brand visible-xs">
 <!-- toggle offscreen menu -->
<div class="toggle-offscreen">
 <a href="#" class="hamburger-icon visible-xs" data-toggle="offscreen" data-move="ltr">
 <span></span>
 <span></span>
 <span></span>
  </a>
</div>
 <!-- /toggle offscreen menu -->
  <!-- logo -->
<div class="brand-logo">
<img src="images/logo-dark.png" height="15" alt="">
 </div>
 <!-- /logo -->
 <!-- toggle chat sidebar small screen-->
<div class="toggle-chat">
 <a href="javascript:;" class="hamburger-icon v2 visible-xs" data-toggle="layout-chat-open">
 <span></span>
 <span></span>
<span></span>
 </a>
 </div> <!-- /toggle chat sidebar small screen-->
</div>
<ul class="nav navbar-nav hidden-xs">
 <li>
  <p class="navbar-text">
 <?php if($_POST["msg"]!="") { ?>
<div id="mydiv" class="alert alert-success" style="width:160%;text-align:center;margin-left:auto;text-transform:uppercase;margin-top:10px;padding:5px !important">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
<strong><?php echo $_POST["msg"];?></strong> 
 </div>
<?php } ?> 
 </p>
 </li>
</ul>
 <ul class="nav navbar-nav navbar-right hidden-xs">
 <li>
 <a href="javascript:;" data-toggle="dropdown">
 <img src="images/avatar.jpg" class="header-avatar img-circle ml10" alt="user" title="user">
<span class="pull-left"><?php //$admin_rs['first_name']." ".$admin_rs['last_name']?> <?php echo $admin_rs['first_name']; ?> </span>
 </a>
 <ul class="dropdown-menu">
  <li>
   <a href="profile-setting.php">Profile setting</a>
 </li>
 <li>
  <a href="logout.php">Logout</a>
   </li>
   </ul>
  </li>
 </ul>
 </header>



      <!-- /top header -->