-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 14, 2019 at 02:42 PM
-- Server version: 10.2.24-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `caonweb_expert`
--

-- --------------------------------------------------------

--
-- Table structure for table `services_name`
--

CREATE TABLE `services_name` (
  `id` int(11) NOT NULL,
  `service` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services_name`
--

INSERT INTO `services_name` (`id`, `service`, `status`, `date`) VALUES
(2, 'Company formation', 1, '2018-09-20'),
(3, 'Company secretarial', 1, '2018-09-20'),
(4, 'Bookkeeping', 1, '2018-12-11'),
(5, 'Auditing', 1, '2018-09-20'),
(6, 'Statutory Reporting', 1, '2018-09-20'),
(7, 'Business Startup consultancy', 1, '2018-09-20'),
(8, 'Business consultancy', 1, '2018-09-20'),
(9, 'Business analysis', 1, '2018-09-20'),
(10, 'Foreign Remittance compliance (15CA/CB, RBI filings)', 1, '2018-09-20'),
(11, 'Tax Planning', 1, '2018-09-20'),
(12, 'Company Closure', 1, '2018-09-20'),
(13, 'Registered Address Change of a company', 1, '2018-09-20'),
(14, 'Business Recovery', 1, '2018-09-20'),
(15, 'Business Valuations', 1, '2018-09-20'),
(17, 'Due Diligence', 1, '2018-09-20'),
(18, 'Family business consulting', 1, '2018-09-20'),
(19, 'Financial Planning', 1, '2018-09-20'),
(20, 'Financial Reporting', 1, '2018-09-20'),
(21, 'Income Tax consulting', 1, '2018-09-20'),
(22, 'Change in Share capital of company', 1, '2018-09-20'),
(23, 'Appointment and resignation of directors', 1, '2018-09-20'),
(24, 'E-commerce startup consulting', 1, '2018-09-20'),
(25, 'Wealth creation and management', 1, '2018-09-20'),
(26, 'Shop License', 1, '2018-09-20'),
(27, 'GST Consulting', 1, '2018-09-20'),
(28, 'TDS Consulting', 1, '2018-09-20'),
(29, 'Payroll Consulting', 1, '2018-09-20'),
(30, 'Investment Advisory', 1, '2018-09-20'),
(55, 'Tax Filing Expert', 1, '2018-12-28'),
(32, 'Foreign Taxation', 1, '2018-09-20'),
(33, 'FDI Advisory', 1, '2018-09-20'),
(34, 'Income Tax Return', 1, '2018-09-20'),
(35, 'Risk management Advisory', 1, '2018-09-20'),
(36, 'Food License', 1, '2018-09-20'),
(37, 'APEDA Registration', 1, '2018-09-20'),
(38, 'Trademark registration', 1, '2018-09-20'),
(39, 'Organic Product Certification', 1, '2018-09-20'),
(40, 'Startup Recognition', 1, '2018-09-28'),
(41, 'CA Certification', 1, '2018-12-05'),
(42, 'Company Annual filing', 1, '2018-12-05'),
(43, 'LLP Annual Filing', 1, '2018-12-05'),
(44, 'IEC Registration', 1, '2018-12-05'),
(45, 'MSME Registration', 1, '2018-12-11'),
(46, 'NIR Registration', 1, '2018-12-05'),
(47, 'DSC', 1, '2018-12-11'),
(48, 'ISO Registration', 1, '2018-12-05'),
(49, 'Trust Formation', 1, '2018-12-05'),
(50, 'ROC Filing', 1, '2018-12-06'),
(51, 'FSSAI Registration', 1, '2018-12-11'),
(52, 'GST Return', 1, '2018-12-11'),
(53, 'Other', 1, '2018-12-06'),
(56, 'GST Registration Filing', 1, '2018-12-28'),
(63, 'Director kyc', 1, '2019-02-14'),
(58, 'Book keeping Outsourcing', 1, '2018-12-28'),
(59, 'Statutory Voluntary audits', 1, '2018-12-28'),
(60, 'Company Formation Registration', 1, '2018-12-28'),
(61, 'FDI Compliance', 1, '2018-12-28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `services_name`
--
ALTER TABLE `services_name`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `services_name`
--
ALTER TABLE `services_name`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
