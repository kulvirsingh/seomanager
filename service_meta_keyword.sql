-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 14, 2019 at 02:43 PM
-- Server version: 10.2.24-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `caonweb_expert`
--

-- --------------------------------------------------------

--
-- Table structure for table `service_meta_keyword`
--

CREATE TABLE `service_meta_keyword` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `service_name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_meta_keyword`
--

INSERT INTO `service_meta_keyword` (`id`, `service_id`, `service_name`, `title`, `keyword`, `description`, `status`, `date`) VALUES
(4, 63, 'Director kyc', 'Find KYC experts | Find a Directory KYC Professional Near You', 'Find KYC experts, Directory KYC Professional Near You', 'Find, compare and select KYC experts in India. Read reviews of Director KYC providers. CAONWEB - India&#039;s leading online directory for finding CA CS.', 1, '2019-04-29'),
(5, 61, 'FDI Compliance', 'Find FDI experts | Find a FDI Professional Near You', 'FDI experts, FDI Professional Near You ', 'Find, compare and select FDI experts in India. Read reviews of FDI providers. CAONWEB - India&#039;s leading online directory for finding CA CS.', 1, '2019-04-29'),
(6, 60, 'Company Formation Registration', 'Find Company Formation experts | Find a Company Formation Professional Near You', 'Company Formation Professional Near You, Company Formation experts', 'Find, compare and select Company Formation experts in India. Read reviews of Company Formation providers. CAONWEB - India&#039;s leading online directory for finding CA CS.\r\n\r\n', 1, '2019-04-29'),
(7, 58, 'Book keeping Outsourcing', 'Find an Accountant or Bookkeeper Near You', 'Bookkeeper Near You, Find an Accountant ', 'Use CAONWEB directory to find a small business accountant, bookkeeper or financial advisor near you.', 1, '2019-04-29'),
(8, 56, 'GST Registration Filing', 'Find GST Expert Near You | GST Tax Consultants | CAONWEB', 'gst consultant online, tax consultant, tax practitioners, gst practitioner, accountant online, tax return preparer, find gst practitioner, find gstp', 'Best GST practitioners profiles created by CAONWEB. Now manage your GST Registration nonfidently. Get EXPERT assistance from our network of GST professionals. Locate a GST Consultant near you now!', 1, '2019-04-29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `service_meta_keyword`
--
ALTER TABLE `service_meta_keyword`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `service_meta_keyword`
--
ALTER TABLE `service_meta_keyword`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
