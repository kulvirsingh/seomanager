<?php

/*_______________________________________________________________________

Created By	: Mrityunjay

Modified By : 18-nov-2013

Modified On : 

Description : This file consists all the config varibales and settings used in the site

_________________________________________________________________________

*/

@session_start();

/****************  Error Handling Code starts *******************************************/

define ("FATAL","E_USER_ERROR");

define ("ERROR","E_USER_WARNING");

define ("WARNING","E_USER_NOTICE");

error_reporting(1);

ini_set('display_errors', 1);

/****************  Error Handling Code starts *******************************************/

/****************  Following code will be for the Cross Site Scripting Starts **************/

if (!empty($_POST)) {

	reset($_POST);

	while (list($k,$v)=each($_POST)) {

	if(!is_array($_POST{$k}))

		$_POST{$k}=htmlentities($v,ENT_QUOTES);

	}	

}

if (!empty($_GET)) {

	reset($_GET);

	while (list($k,$v)=each($_GET)) 

	{

		if(!is_array($_GET{$k}))

			$_GET{$k}=htmlentities($v,ENT_QUOTES);

	}	

}

if (!empty($_REQUEST)) {

	reset($_REQUEST);

	while (list($k,$v)=each($_REQUEST)) 

	{

		if(!is_array($_REQUEST{$k}))

			$_REQUEST{$k}=htmlentities($v,ENT_QUOTES);

	}	

}

/**************** code for will be for the Cross Site Scripting ends **************

/****************  Some commn settings starts *********************************************/

define("ROOT_FOLDER","/") ;

define("DOCUMENTROOT",$_SERVER['DOCUMENT_ROOT'].ROOT_FOLDER);

$name_arr = explode("/",$_SERVER["SCRIPT_FILENAME"]);

if(in_array("seomanager",$name_arr)==true)

{

	define("PATH","../");

	define("FOLDER_PATH",PATH."codelibrary/inc/");

}

else

{

	define("PATH","");

	define("FOLDER_PATH","codelibrary/inc/");

}

define('APP_ID', '856136511126712');

define('APP_SECRET', 'b670622d5bf824bf10a683a3f00ef530');

define("UPLOADS_PATH", "http://".$_SERVER['HTTP_HOST'].ROOT_FOLDER."uploads/");

define("CSS_PATH", "http://".$_SERVER['HTTP_HOST'].ROOT_FOLDER."css/");

define("JS_PATH", "http://".$_SERVER['HTTP_HOST'].ROOT_FOLDER."js/");

define("IMAGE_PATH", "http://".$_SERVER['HTTP_HOST'].ROOT_FOLDER."images/");

define("SITE_PATH", "http://".$_SERVER['HTTP_HOST'].ROOT_FOLDER);

define("SITE_ADMIN_PATH", "http://".$_SERVER['HTTP_HOST'].ROOT_FOLDER."seomanager/");





$AdminEmail = "kulvirsingh199@gmail.com";

define("PAGING_SIZE",15);

define("EVEN_PAGING_SIZE",20);

define("TOTAL_RATING_SIZE",1);



/******************** Different Status used in the site **************************************/

$GL_active=1;

$GL_not_active=0; //deactive status

/******************** Different Status used in the site **************************************/



		

$tdClass1="evenRow";   // Class name of style sheet for listing page

$tdClass2="oddRow";	// Class name of style sheet for listing page



$frmtdClass1="evenRow";   // Class name of style sheet for listing page

$frmtdClass2="oddRow";	// Class name of style sheet for listing page



$frmtdClassExt1="evenRowext";   // Class name of style sheet for listing page extra for admin

$frmtdClassExt2="oddRowext";	// Class name of style sheet for listing page extra for admin





$tdManageClass1="listingManageBg2";	// Class name of style sheet for listing page

$tdManageClass2="listingManageBlueBg1";   // Class name of style sheet for listing page



$tdManageClassSep1="manage_listingWht_Sep.gif";	// Class name of style sheet for listing page

$tdManageClassSep2="manage_listingBlue_Sep.gif";   // Class name of style sheet for listing page



		



/**************************  Activate/Deavtivate Image variables **************************/

$ad_active_image="<img src=\"".ADMIN_IMAGE_PATH."active.gif\">";

$ad_deactive_image="<img src=\"".ADMIN_IMAGE_PATH."deactive.gif\">";

$imageUrl=$site_url.IMAGE_PATH."/"; // images URL

/**************************  Activate/Deavtivate Image variables **************************/

$ext = "../";





//$AdminEmail = "admin@sitename.com";



/***********************   General settings variables   **********************************/

/******************************  Array Section Start       ******************************/

//user paging dropdown array

$arr_user_paging[5]="5 Per Page";

$arr_user_paging[10]="10 Per Page";

$arr_user_paging[15]="15 Per Page";

$arr_user_paging[20]="20 Per Page";

$arr_user_paging[50]="20 Per Page";

/* ---------------  Array Section Start--------------------------------------------*/



//error message/message array

$arr_error_msg=Array();

$arr_error_msg[0]="Error on saving data.";

$arr_error_msg[1]="Invalid Username/Password.";

$arr_error_msg[2]="This email address already exists for other user";

$arr_error_msg[3]="Invalid old password.";

$arr_error_msg[4]="Invalid E-mail address.";





//confirmation message array

$arr_con_msg=Array();

$arr_con_msg[0]="";

$arr_con_msg[1]="You have been logout successfully.";

$arr_con_msg[2]="Email address changed successfully.";

$arr_con_msg[3]="Password has been changed Successfully.";

$arr_con_msg[4]="Category has been added successfully.";

$arr_con_msg[5]="Category has been updated successfully.";

// salutation

$paging = 10;              //10 by default 

$user_paging = 5;              //5 by default

$admin_paging = 10;              //5 by default

$fifty_records_paging = 50;



$GL_salutation = array("Mr.","Mrs","Ms");

$GL_default_date_format = 'd/m/Y';

$GL_access_level = array("admin","user");

$GL_adminEmail = "kulvirsingh199@gmail.com";

$GL_user_paging     = 30;

$GL_product_paging  = 40 ;

$GL_category_paging = 30 ;

$GL_date_format = "d-m-Y"; 

$GL_currency  = "$" ;

$GL_pay_success  =  "http://".$_SERVER['HTTP_HOST'].ROOT_FOLDER.'pay-success.php' ;

$GL_pay_cancel  = "http://".$_SERVER['HTTP_HOST'].ROOT_FOLDER.'pay-cancel.php' ;

// Global Property type

$GL_user[1] = "Dealer" ;

$GL_user[2] = "User" ;

function __autoload($class_name) {

   require_once FOLDER_PATH.strtolower($class_name).'.php';

}

?>